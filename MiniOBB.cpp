#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Polyhedron_3.h>
#include <iostream>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <fstream>
#include <vector>
#include <Eigen/Dense>

typedef CGAL::Simple_cartesian<double>     Kernel;
typedef Kernel::Point_3					   Point;
typedef CGAL::Polyhedron_3<Kernel>         Polyhedron;
typedef Polyhedron::Vertex_iterator		   Vertex_iterator;
typedef Polyhedron::Point_iterator		   Point_iterator;
typedef Polyhedron::Halfedge_handle		   Halfedge_handle;
using namespace std;
using namespace Eigen;

#define ROTATE(a,i,j,k,l) g=a(i + 4 * j); h=a(k + 4 * l); a(i + 4 * j)=(float)(g-s*(h+g*tau)); a(k + 4 * l)=(float)(h+s*(g-h*tau));

static Matrix4f GetConvarianceMatrix(const MatrixXf & vertPos, unsigned int vertCount)
{
	typedef unsigned int uint;
	uint i;
	Matrix4f cov = Matrix4f::Zero();

	double S1[3] = { 0.0 };
	double S2[3][3];
	//const uint vertCount = vertPos.size();
	S2[0][0] = S2[1][0] = S2[2][0] = 0.0;
	S2[0][1] = S2[1][1] = S2[2][1] = 0.0;
	S2[0][2] = S2[1][2] = S2[2][2] = 0.0;

	// get center of mass 
	for (i = 0; i < vertCount; i++)
	{
		S1[0] += vertPos(i, 0);
		S1[1] += vertPos(i, 1);
		S1[2] += vertPos(i, 2);

		S2[0][0] += vertPos(i, 0) * vertPos(i, 0);
		S2[1][1] += vertPos(i, 1) * vertPos(i, 1);
		S2[2][2] += vertPos(i, 2) * vertPos(i, 2);
		S2[0][1] += vertPos(i, 0) * vertPos(i, 1);
		S2[0][2] += vertPos(i, 0) * vertPos(i, 2);
		S2[1][2] += vertPos(i, 1) * vertPos(i, 2);
	}

	const float n = static_cast<float>(vertCount);
	// now get covariance
	cov(0) = (float)(S2[0][0] - S1[0] * S1[0] / n) / n;
	cov(5) = (float)(S2[1][1] - S1[1] * S1[1] / n) / n;
	cov(10) = (float)(S2[2][2] - S1[2] * S1[2] / n) / n;
	cov(4) = (float)(S2[0][1] - S1[0] * S1[1] / n) / n;
	cov(9) = (float)(S2[1][2] - S1[1] * S1[2] / n) / n;
	cov(8) = (float)(S2[0][2] - S1[0] * S1[2] / n) / n;

	cov(1) = cov(4);
	cov(2) = cov(8);
	cov(6) = cov(9);

	cout << "result: " << endl << cov << endl;
	return cov;
}

static float& GetElement(Vector3f & point, int index)
{
	if (index == 0)
		return point(0);
	
	if (index == 1)
		return point(1);
	
	if (index == 2)
		return point(2);

	//	CC_ASSERT(0);  
	return point(0);
}

static void GetEigenVectors(Matrix4f & vout, Vector3f & dout, Matrix4f &a)
{
	int n = 3;
	int j, iq, ip, i;
	double tresh, theta, tau, t, sm, s, h, g, c;
	int nrot;
	Vector3f b, z, d;
	Matrix4f v = Matrix4f::Identity();

	for (ip = 0; ip < n; ip++)
	{
		GetElement(b, ip) = a(ip + 4 * ip);
		GetElement(d, ip) = a(ip + 4 * ip);
		GetElement(z, ip) = 0.0;
	}

	nrot = 0;

	for (i = 0; i < 50; i++)
	{
		sm = 0.0;

		for (ip = 0; ip < n; ip++) 
			for (iq = ip + 1; iq < n; iq++) 
				sm += fabs(a(ip + 4 * iq));

		if (fabs(sm) < FLT_EPSILON)
		{
			Matrix4f v_tmp = v.transpose();
			v = v_tmp;
			vout = v;
			dout = d;
			
			return;
		}

		if (i < 3)
			tresh = 0.2 * sm / (n*n);
		else
			tresh = 0.0;

		for (ip = 0; ip < n; ip++)
		{
			for (iq = ip + 1; iq < n; iq++)
			{
				g = 100.0 * fabs(a(ip + iq * 4));
				float dmip = GetElement(d, ip);
				float dmiq = GetElement(d, iq);

				if (i>3 && fabs(dmip) + g == fabs(dmip) && fabs(dmiq) + g == fabs(dmiq))
				{
					a(ip + 4 * iq) = 0.0;
				}
				else if (fabs(a(ip + 4 * iq)) > tresh)
				{
					h = dmiq - dmip;
					if (fabs(h) + g == fabs(h))
					{
						t = (a(ip + 4 * iq)) / h;
					}
					else
					{
						theta = 0.5 * h / (a(ip + 4 * iq));
						t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
						
						if (theta < 0.0) 
							t = -t;
					}

					c = 1.0 / sqrt(1 + t*t);
					s = t*c;
					tau = s / (1.0 + c);
					h = t * a(ip + 4 * iq);
					GetElement(z, ip) -= (float)h;
					GetElement(z, iq) += (float)h;
					GetElement(d, ip) -= (float)h;
					GetElement(d, iq) += (float)h;
					a(ip + 4 * ip) = 0.0;

					for (j = 0; j < ip; j++) 
					{ 
						ROTATE(a, j, ip, j, iq); 
					}
					
					for (j = ip + 1; j < iq; j++)
					{
						ROTATE(a, ip, j, j, iq); 
					}
					
					for (j = iq + 1; j < n; j++)
					{ 
						ROTATE(a, ip, j, iq, j); 
					}

					for (j = 0; j < n; j++)
					{ 
						ROTATE(v, j, ip, j, iq); 
					}

					nrot++;
				}
			}
		}

		for (ip = 0; ip < n; ip++)
		{
			GetElement(b, ip) += GetElement(z, ip);
			GetElement(d, ip) = GetElement(b, ip);
			GetElement(z, ip) = 0.0f;
		}
	}

	Matrix4f v_temp = v.transpose();
	v = v_temp;
	vout = v;
	dout = d;

	return;
}

static Matrix4f GetOBBOrientation(const MatrixXf & vertPos, int num)
{
	Matrix4f Cov; //创建一个 4*4 矩阵  

	if (num <= 0)
		return Matrix4f::Identity(); //返回单位矩阵  

	Cov = GetConvarianceMatrix(vertPos, num); //创建协方差矩阵  

	// now get eigenvectors  
	Matrix4f Evecs = Matrix4f::Zero();
	Vector3f Evals;

	GetEigenVectors(Evecs, Evals, Cov); //求特征向量  

	Matrix4f Evecs_temp = Evecs.transpose();
	Evecs = Evecs_temp;

	return Evecs;
}

Polyhedron::Halfedge_handle make_cube_3( Polyhedron & P ,vector<Point> & pts) 
{
	// appends a cube of size [0,1]^3 to the polyhedron P.
	CGAL_precondition( P.is_valid());
	typedef Polyhedron::Point_3         Point;
	typedef Polyhedron::Halfedge_handle Halfedge_handle;
	Halfedge_handle h = P.make_tetrahedron( pts[2],pts[0],pts[1],pts[6] );
	Halfedge_handle g = h->next()->opposite()->next();             // Fig. (a)
	P.split_edge( h->next());
	P.split_edge( g->next());
	P.split_edge( g);                                              // Fig. (b)
	h->next()->vertex()->point()     = pts[3];
	g->next()->vertex()->point()     = pts[7];
	g->opposite()->vertex()->point() = pts[5];            // Fig. (c)
	Halfedge_handle f = P.split_facet( g->next(),
		g->next()->next()->next()); // Fig. (d)
	Halfedge_handle e = P.split_edge( f);
	e->vertex()->point() = pts[4];                        // Fig. (e)
	P.split_facet( e, f->next()->next());                          // Fig. (f)
	CGAL_postcondition( P.is_valid());
	return h;
}

Eigen::Vector3f Mat4Vec3(Matrix4f & m , Vector3f & v)
{
	Vector3f re;
	re(0) =  v(0) * m(0)+ v(1) * m(4) + v(2) * m(8);
	re(1) = v(0) * m(1) + v(1) * m(5) + v(2) * m(9);
	re(2) = v(0) * m(2) + v(1) * m(6) + v(2) * m(10);
	return re;
}

Eigen::RowVector3f Mat4Vec3(Matrix4f & m , RowVector3f & v)
{
	RowVector3f re;
	re(0) =  v(0) * m(0)+ v(1) * m(4) + v(2) * m(8);
	re(1) = v(0) * m(1) + v(1) * m(5) + v(2) * m(9);
	re(2) = v(0) * m(2) + v(1) * m(6) + v(2) * m(10);
	
	return re;
}

void OBB(const MatrixXf & verts, int num)
{

	Matrix4f matTransform = GetOBBOrientation(verts, num); //创建包围盒取向矩阵  

	/*
	matTransform是一个正交矩阵,所以它的逆矩阵就是它的转置;
	AA'=E（E为单位矩阵，A'表示“矩阵A的转置矩阵”） A称为正交矩阵
	*/
	Matrix4f mat_temp = matTransform.transpose(); //计算matTransform矩阵的转置(此处相当于求逆矩）  
 	matTransform = mat_temp;
	Vector3f vecMax = Mat4Vec3(matTransform,Vector3f(verts(0, 0), verts(0, 1), verts(0, 2)));
	Vector3f vecMin = vecMax;

	for (int i = 1; i < num; i++)
	{
		Vector3f vect = Mat4Vec3(matTransform,Vector3f(verts(i, 0), verts(i, 1), verts(i, 2)));
		vecMax(0) = vecMax(0) > vect(0) ? vecMax(0) : vect(0);
		vecMax(1) = vecMax(1) > vect(1) ? vecMax(1) : vect(1);
		vecMax(2) = vecMax(2) > vect(2) ? vecMax(2) : vect(2);

		vecMin(0) = vecMin(0) < vect(0) ? vecMin(0) : vect(0);
		vecMin(1) = vecMin(1) < vect(1) ? vecMin(1) : vect(1);
		vecMin(2) = vecMin(2) < vect(2) ? vecMin(2) : vect(2);
	}

	Matrix4f mat_temp_2 = matTransform.transpose();
	matTransform = mat_temp_2;

	Vector3f xAxis = Vector3f(matTransform(0), matTransform(1), matTransform(2));
	Vector3f yAxis = Vector3f(matTransform(4), matTransform(5), matTransform(6));
	Vector3f zAxis = Vector3f(matTransform(8), matTransform(9), matTransform(10));

	RowVector3f center = 0.5 * (vecMax + vecMin);
	
	center = Mat4Vec3(matTransform,center);
	
	cout << "Center : " << center << endl;
	
	xAxis.normalize();
	yAxis.normalize();
	zAxis.normalize();
	Vector3f extents = 0.5 *(vecMax - vecMin);

	RowVector3f extX = xAxis * extents(0);
	RowVector3f extY = yAxis * extents(1);
	RowVector3f extZ = zAxis * extents(2);
	RowVector3f vert_box[8];
	vert_box[0] = center - extX + extY + extZ;     // left top front
	vert_box[1] = center - extX - extY + extZ;     // left bottom front
	vert_box[2] = center + extX - extY + extZ;     // right bottom front
	vert_box[3] = center + extX + extY + extZ;     // right top front
	vert_box[4] = center + extX + extY - extZ;     // right top back
	vert_box[5] = center + extX - extY - extZ;     // right bottom back
	vert_box[6] = center - extX - extY - extZ;     // left bottom back
	vert_box[7] = center - extX + extY - extZ;     // left top back
	Polyhedron box;
	vector<Point> point_box;
	for(int i = 0 ; i < 8 ; i++)
	{
		point_box.push_back(Point(vert_box[i](0),vert_box[i](1),vert_box[i](2)));

	}
	make_cube_3(box,point_box);
	ofstream file_o = ofstream("box.off");
	file_o << box;
	file_o.close();
	// 	cout <<"Axis : "<< xAxis<<yAxis<<zAxis <<endl;
	//       
	//     Vector3f extents = 0.5 * (vecMax - vecMin);  
}

int main()
{
	Polyhedron mesh ; 
	string file;
	cin >> file;
	ifstream input = ifstream(file);
	input >> mesh;
	MatrixXf points;
	vector<Point> tt;
	int i = 0 ;
	points.resize(mesh.size_of_vertices(),3);
	RowVector3f cen = RowVector3f(0.0,0.0,0.0);
	const int nb_p = mesh.size_of_vertices();

	for(Point_iterator xx = mesh.points_begin();
		xx!=mesh.points_end();xx++)
	{
		cen(0) += xx->x() / static_cast<double>(nb_p);
		cen(1) += xx->y() / static_cast<double>(nb_p);
		cen(2) += xx->z() / static_cast<double>(nb_p);
	}
	cout << "We calculate the center is "<< cen<<endl;

	for(Point_iterator it = mesh.points_begin();
		it != mesh.points_end(); it++ ,i++ )
	{
		points(i,0) = it->x();
		points(i,1) = it->y();
		points(i,2) = it->z();
		

	}
	system("pause");
	printf("size = %d\n vertices = %d\n",points.size(),mesh.size_of_vertices());
	//cout << points << endl;
	OBB(points,mesh.size_of_vertices());
	
	//cout << "We calculate the center is "<< cen<<endl;
	system("pause");
	return 0;

}